<html style="cursor: url(&quot;chrome://grabanddrag/skin/grab.png&quot;) 10 4, move ! important;"><head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8"><title>LaTeX Tips: Document classes and packages 
</title></head>

<body>

<h1>LaTeX tips: Document classes and packages</h1>

<h2>
The article/book and amsart/amsbook document classes
</h2>

Unless a publisher specifically asks to use in-house style files, it
is recommended that for a paper or book
you use one of the standard document classes that
are part any current TeX installation: 
<code>article</code> or <code>amsart</code> for papers, and
<code>book</code> or <code>amsbook</code> for book-length manuscripts.
The main difference between the <code>article/book</code> classes and
the <code>amsart/amsbook</code> classes (which are part of the amslatex
package) is in the "look" of the document. The non-ams classes   
set the title matter and section headings in a larger font, which some 
people find more attractive. 
<p>
</p><h3>Differences between the article/book classes and the amsart/amsbook
classes</h3>
To a large degree, the two pairs of classes are interchangeable, so
it is easy to switch from one class to another after a paper is
finished.  The body of the paper does not have to be changed,
but some editing in the title matter is usually needed to account 
for differences in the handling of title items by these classes:
<ul>
<li>
<b>In the ams classes, the abstract goes before the \maketitle
command; in the article/book classes, it goes after \maketitle.
</b>
</li><li>
<b>When using article/book, add \usepackage{amsmath, amsthm} 
to make the amslatex commands available.</b> 
The ams document classes load these packages automatically, so no
explicit loading is needed. 
</li><li>
<b>The article/book classes allow footnotes in the
title matter, the ams classes do not.</b> In 
<code>article/book</code> class, one can put footnotes to
the author or title
containing, for example, a grant acknowledgement, a subject
classification, or the author's address. 
This causes problems in the ams classes; with the ams classes you have
to use the  commands <code>\thanks{...}</code> (for acknowledgements),
<code>\subjclass{...}</code>, and <code>\address{...}</code> to
provide this information. 
</li><li>
<b>In the article/book classes equation numbers are set by default
on the right of the page; in the ams classes they are set on the left
of the page.
</b>
This behavior can be changed by specifying the
option "reqno" or "leqno" (see below). 
</li></ul>
<p>
</p><h3>Useful document class options</h3>
Options to document classes are placed brackets between the
<code>\documentclass</code> command and the name of the document
class; multiple options should be placed in a single pair of brackets,
separated by commas; for example,
<code>\documentclass[leqno,draft]{article}</code>.

Here are some useful options that work with all of the mentioned
paper and book classes:
<ul>
<li>
<b>[draft]</b>  The main effect of this option is to indicate overfull
hbox by a vertical bar in the margin; this helps locating overfull
hboxes reported in the log file.  Another effect of the 
<code>draft</code> option is that embedded pictures do not get
displayed; instead, only an outline of the box taken up by the picture
is shown.
</li><li>
<b>[11pt], [12pt].</b> These options
changes the font size from the default of 10pt to 11pt or 12pt, and
cause other size parameters to be similarly increased; the effect of
this option is similar to that of the <code>\magnification</code>
command in plain tex. (<code>\magnification</code> does not work in
latex.)  
Using this option 
is the proper way to "magnify" a document. Don't try to magnify fonts
manually, using fontsizing commands like <code>\larger</code>; since such
commands only affect a single font and leave other fonts as well as 
spacing parameters unchanged, this results in a poor looking
document.  
</li><li>
<b>[leqno], [reqno].</b> Use one of these options to change the
placement of equation numbers from the default (left in the
<code>amsart/amsbook</code>
classes, right in the 
<code>article/book</code> classes)  
</li><li>
<b>[a4paper].</b> This sets the paper size to A4, the standard in most
European countries.  The default (at least in our TeX installation) is
US letter size, so it's unlikely you'll need this option, but 
papers originating in Europe often are prepared with this option turned on,
and processing a tex file with  this option may cause difficulties
when trying to print it since pages may to be too long to be long to
fit on letter size paper. This is easily fixed by deleting the a4paper
option in the source file.
</li></ul>

<h2>
The slides document class
</h2>
The slides document class is one of two standard ways to typeset material
in a form that is appropriate for putting on transparencies (the other being
<b>foiltex</b>).  It  causes the document to be substantially 
magnified (while keeping the overall dimensions of the page), so that it is 
suitable for copying onto transparencies.
In most respects, the <code>slides</code>
class works like the <code>article</code> class, but there is one
major difference to be aware of: The <code>\pagestyle</code> commands
do not work in the <code>slides</code> class; if you want to use 
customized headers or footers, use the <code>fancyhdr</code> package
(see below). 


<h2>
LaTeX packages
</h2>

LaTeX "packages" serve the same function as  
libraries for programming languages: 
a package contains a set of commands that are not built into the
core of LaTeX, but useful for special purposes. 
There exist hundreds 
of packages; many (including all of those listed below)
are part of any standard TeX installation, while others can be
downloaded from the 
CTAN (Comprehensive TeX Archive Network) depository at  
<a href="http://ctan.tug.org/"><tt>http://ctan.tug.org</tt></a>.
An excellent overview of common packages is contained in the 
<a href="file:///usr/local/encap/teTeX/share/texmf/doc/index.html">teTeX
documentation</a>. (This link works only for local users.) 
<p>
To load a package,
add a <code>\usepackage{...}</code> instruction
after <code>\documentclass</code>, with the name of the package in
braces. Multiple packages can be included in a single
<code>\usepackage{...}</code> statement, separated by commas; for
example: <code>\usepackage{amsmath, amsthm}</code>.
</p><p>
Here are some packages that are commonly used in mathematical writing. 

</p><ul>
<li><b>amsmath, amsthm:</b>
These packages, which are part of 
amslatex, are needed to make the amslatex enhancements for typesetting
equations and theorems available. The packages are loaded
automatically if you use one of the ams document classes; if you use a
standard LaTeX document class, load the packages explicitly with
<code>\usepackage{amsmath, amsthm}</code>.
<p>
<b>Documentation.</b> 
Gratzer's book "Math into LaTeX" provides complete documentation for  
these and other  amslatex packages, and is the only book to do so.
Online documentation exists in the form of the <a href="http://www.ams.org/tex/short-math-guide.html">"Short Math Guide for
LaTeX"</a>. This is a reference manual, rather than an 
introduction or tutorial. 
</p><p>
</p></li><li><b>amssymb:</b> This package is part of
the amslatex distribution and loads additional fonts and symbols.
In particular, it loads the <code>amsfonts</code> package which, among
other things, makes 
blackboard bold (<code>\mathbb</code>) and fraktur
(<code>\mathfrak</code>) fonts
available. The  <code>amssymb</code> also includes a number of
additional symbols or variants of standard LaTeX symbols, such as a
slanted "less than or equal" sign. (However, I would recommend using
the standard versions, unless there is a particular reason to use the
variant.) 
<p>
<b>Documentation:</b> 
A complete listing of the symbols provided by <code>amssymb</code>, 
is given in Appendix A of Gratzer's book, and in 
the <a href="http://www.ams.org/tex/short-math-guide.html">Short Math Guide for
LaTeX</a>, mentioned above.
</p><p>
</p></li><li><b>amscd:</b> Also part of the amslatex distribution, this is a
package for typesetting simple commutative diagrams. It is a lightweight,
easy-to-use package, but it cannot handle  diagrams with diagonal
arrows; if you need to draw diagrams with diagonal arrows, use
the <code>xy</code> package described below. 
<p> <b>Documentation:</b> Section 5.8 of Gratzer's book.   
</p><p>
</p></li><li><b>xy:</b> This is the name of the package that loads the "xy-pic" program, 
a general drawing package for TeX that is especially useful for
drawing complex commutative diagrams.  This is a real heavyweight
among drawing packages, and amazingly powerful and versatile; it can
do all sorts of tricks, including looping around nodes in a
commutative diagram, looping an arrow over or under another arrow 
to create the impression of a 3D like image, and much more. It is hard
to image a commutative diagram that <code>xy</code> wouldn't be able
to handle.
<p>
<b>Documentation.</b> 
There is a short guide, 
<a href="file:///usr/local/encap/teTeX/share/texmf/doc/generic/xypic/xyguide.ps">xyguide.ps</a>
and a comprehensive (81 pages) reference manual
<a href="file:///usr/local/encap/teTeX/share/texmf/doc/generic/xypic/xyrefer.ps">xyrefer.ps</a>.  
(Both links work only for local users; you can also access the
documentation with the "texdoc" command which should work 
on most standard TeX installations: "texdoc xyguide" or "texdoc
xyrefer".) 
Despite the size of the reference manual, for simple diagrams
<code>xy</code> is fairly easy to use, and you will
likely get by using just the short guide.
</p><p>
</p></li><li><b>graphicx:</b> 
The <code>graphicx</code> package allows inclusion of
graphics files produced by other aplications (such as
the free program <code>xfig</code>, available on Unix systems,
Mathematica/Maple, 
or commercial software like CorelDraw). (Note the "x" at the end;
the precursor to <code>graphicx</code>
is a package called <code>graphics</code>, but this
package is obsolete and should not be used.) 
The graphics files must be in eps (encapsulated postscript) format, so
be sure to save the files in this format. (All of the mentioned
applications can do this.)
<p>
<b>Documentation.</b> The main source of documentation is the
document "Using imported graphics in LaTeX2e", which, despite its
hefty size (86 pages), provides an excellent introduction to the
<code>graphicx</code> package, and 
to the issues involved in using graphical material in TeX files.
The <code>graphicx</code> package is also documented in the "LaTeX
graphics companion". 
</p><p>
<b>Usage.</b>
The <code>graphicx</code> package has numerous features and options,
but the basic use is very simple: 
First load the <code>graphicx</code> package with
<code>\usepackage{graphicx}</code>,  and then  use 
the <code>\includegraphics{...}</code> command (note the "s" at the
end!) for every file you want to import into your LaTeX file.
In its simplest form, you just use the <code>\includegraphics</code>
command with 
the file name given inside the braces. However, you will usually 
want to place this command inside a <code>figure</code> environment, 
and probably also wrap it inside <code>\begin{center} ... 
\end{center}</code> so that the graphics is horizontally centered.  
Furthermore, you may need to scale the graphics so that it fits the
page; this can be accomplished with an optional <code>width</code>
argument, 
which sets the width to a specific length (and also scales the height
proportionally).
</p><p>
Here is a simple, complete example, showing how to include a file, 
plot.eps, in a document. Note that the .eps extension need not be
specified.

</p><pre><code>

\documentclass{article}
\usepackage{graphicx}

\begin{document}

\begin{figure}[tbh]
\begin{center}
\includegraphics[width=.8\textwidth]{plot}
\end{center}
\caption{Plot of the function $\sin x$}
\label{sineplot}
\end{figure}

\end{document}
</code>
</pre>
Here are some additional hints when using this package.
<p>
</p><ul>
<li>
<b>graphicx under the draft mode.</b>
When the "draft" option in the <code>\documentclass</code> command 
is turned on, the graphics files are not displayed;   
only an
outline of the rectangular boxes containing the graphics is shown. 
This is useful in order to determine the proper sizing 
and placement of the graphics. To override this behavior, 
use the option "final" when loading the package: 
 <code>\usepackage[final]{graphicx}.</code> 
With this option, all graphics  will be displayed, regardless of
whether or not the draft option is turned on.
<p>
</p></li><li>
<b>Graphics file extensions.</b>
The .eps extension in the eps files need not be specified (but be sure
that there is a file with this extension in the current directory).
In fact, leaving out the .eps extension has the advantage that 
the same LaTeX source file can be used by the pdflatex program to 
create a pdf version of the document. 
This requires that the graphics are available in both eps and 
pdf formats.  (You can use conversion programs,  such as "epstopdf",
for that purpose.) 
The pdflatex program expects graphics files to be in pdf format,
whereas latex expects the files to be in eps format.
Making graphics files in both versions available, but 
not specifying a file extension, when including the graphics in the
document, ensures that
each of the two programs picks and finds the version appropriate for
that program.
<p>
</p></li><li>
<b>Placement of figures.</b>
Figures and tables placed inside <code>figure</code> or
<code>table</code> environment are so-called "floats"; this means that
they may be moved further down in 
the document in order to avoid overlong  
pages and similar problems.  LaTeX tries hard to optimize the 
placement, but sometimes this can result in a poor placement, such as in
the middle of a bibliography. This occurs mainly   
in documents containing lots of figures. 
There are several things one can do to 
improve the placement of figures. The first thing to try is to
add an option like "[tbh]" to the <code>\begin{figure}</code> command,
as in the example above. This instructs Tex to try to place the figure
at the top (t) or bottom (b) of the page, or "here" (h), meaning the  
place where the figure occurs in the source code.  If this does not
help, try the more emphatic version "[tbh!]". As a last resort, you
could remove the <code>\begin{figure} ... \end{figure}</code> wrapper
and use manual spacing commands before and after the figure to achieve
the proper spacing. However, this is a drastic measure that is only
recommended if other attempts fail.
</li></ul>

<p>
</p></li><li><b>showkeys:</b>
If your document has a large number of labelled items, remembering
all the labels becomes difficult. The <code>showkeys</code> package
shows these labels explicitly by putting the name of the label 
referenced in a small box placed on the margin of the page, or
directly above the reference.
The labels displayed in this manner include citation labels
(<code>\cite{...}</code>), equation
labels (<code>\eqref{...}</code>), and ordinary labels
(<code>\ref{...}</code>). 
A great tool when working on a complex document. 
<p>
<b>Documentation.</b>
<a href="file:///usr/local/encap/teTeX/share/texmf/doc/latex/tools/showkeys.dvi">
The showkeys package (dvi file).</a> (Local access only. On most
TeX installations, the file can be accessed with the
"texdoc showkeys" command.)
</p><p>
</p></li><li><b>
fancyhdr: </b>
The "Fancy header" package. Allows customizations of footers and
headers.  This greatly extends the capabilities of the
<code>\pagestyle{...}</code> command in standard LaTeX. The package
distinguishes between even and odd numbered pages, 
and for each allows specifying a left/center/right header, and a
left/center/right footer.
<p>
<b>Documentation:</b>
<a href="file:///usr/local/encap/teTeX/share/texmf/doc/latex/fancyhdr/fancyhdr.dvi">
Page Layout in LaTeX (dvi file).</a> (Local access only. On most
TeX installations, the file can also be accessed with the
"texdoc fancyhdr" command.)
</p><p>
Here is an example:
</p><pre><code>
\documentstyle{book}

\usepackage{fancyhdr}
\pagestyle{fancy}

%% L/C/R denote left/center/right header (or footer) elements
%% E/O denote even/odd pages

%% \leftmark, \rightmark are chapter/section headings generated by the 
%% book document class

\fancyhead[LE,RO]{\slshape\thepage}
\fancyhead[RE]{\slshape \leftmark}
\fancyhead[LO]{\slshape \rightmark}
\fancyfoot[LO,LE]{\slshape Short Course on Asymptotics}
\fancyfoot[C]{}
\fancyfoot[RO,RE]{\slshape 7/15/2002}

</code>
</pre>


</li></ul>

<p>
</p><hr>
<a href="http://www.math.uiuc.edu/%7Ehildebr/tex/">
Back to the LaTeX Tips Page</a>
<p>
<i> Last modified: Sat 19 Apr 2003 05:25:48 PM CDT
<a href="http://www.math.uiuc.edu/%7Ehildebr">A.J. Hildebrand</a></i>



</p></body></html>