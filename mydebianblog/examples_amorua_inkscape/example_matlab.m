%% example 4

close all ; clear all; clc;
set(0,'DefaultTextInterpreter', 'none');

%%
fig=myfigure(8); % width = 10cm

x=0:(pi/100):pi;
y=sin(x).*sin(sqrt(120)*x);
plot(x,y,'dr-'); hold on;

xlabel('$t$, s'); 
ylabel('$l$, m');

l=legend('$\sin(x) \sin(\sqrt{120} x)$');
%set(l,'Interpreter','none')

%% custom ticks
set(gca,'XLim',[0 pi],'XTick',[0 pi/4 pi/2 3*pi/4 pi],...
     'XTickLabel',{'$0$','$\pi/4$','$\pi/2$','$3\pi/4$','$\pi$'}...
);
 

% this can be retouched in Inkscape
plot2svg('figs/example_matlab.svg')

