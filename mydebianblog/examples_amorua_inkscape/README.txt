Примеры к

"ЛаТеХ для продвинутых.  Как подружить LaTeX и Inkscape."


- example_matlab.m 
  plot2svg.m
  myfigure.m
      "рисуeт" график в Матлабе,
который затем сохраняется в svg-формате с помощью plot2svg().


- figs/example_matlab.svg 
  figs/example_matlab-retouched.svg 
      график, нарисованный "example_matlab.m" и загруженный в Инкскейп.


- example.tex, example.pdf : TeХ и PDF файлы, демонстрирующие
      как вставить рисунки из Инкскейпа в ЛаТеХ


- svg & pdf/pdf_tex файлы в директории figs/ : нарисованные в
      Inkscape картинки, экспортированные в PDF+TeX формат.

amorua
