%% example 3

close all ; clear all; clc;
set(0,'DefaultTextInterpreter', 'latex');
set(0,'DefaultAxesFontSize',12);
set(0,'DefaultTextFontSize',12);
%%
fig=myfigure(14,7); 

x=0:0.1:pi;
y=sin(x);
plot(x,y,'r'); hold on;

z=cos(x);
plot(x,z,'b');

l=legend('$\sin x$','$\cos x$');


%% Using multiline text via text cell array
xlabel({'$t$, s'; 'time to goal'}); 
ylabel({'$l$, m'; 'distance to goal'});

%i=6;
%plot(x(i),y(i),'or');
%text(x(i)+0.1,y(i),...  % three lines in text()
%   {sprintf('Point $(x,y)=(%3.1f,%3.1f)$',x(i),y(i)); ...
%    'second row, if you wish';...
%    'and even the 3rd one'});


%% Text effects

text(2.5,-0.5,{'Rotated';'text'},'Rotation',90);

text(0.5,-0.5,{'Colored';'centered';'italic';'text'},...
    'Color','r',...
    'HorizontalAlignment','center',...
    'FontAngle','italic');

text(1,-0.5,{'Big Text:';'16pt'},'FontSize',16)

%% use epspad, otherwise it cuts a part of the x- & y-label 
matlabfrag('figs/example3','epspad',[10,22,10,10]);