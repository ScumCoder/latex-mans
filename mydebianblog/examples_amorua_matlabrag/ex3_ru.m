%% example 3

close all ; clear all; clc;
set(0,'DefaultTextInterpreter', 'latex');
set(0,'DefaultAxesFontSize',12);
set(0,'DefaultTextFontSize',12);
%%
fig=myfigure(14,7); 

x=0:0.1:pi;
y=sin(x);
plot(x,y,'r'); hold on;

z=cos(x);
plot(x,z,'b');

l=legend('$\sin x$','$\cos x$');

%% Using multiline text via text cell array

xlabel({'$t$, с'; 'время до цели'}); 
ylabel({'$l$, м'; 'расстояние до цели'});

i=6;
plot(x(i),y(i),'or');
text(x(i)+0.1,y(i),...  % three lines in text()
   {sprintf('Точка $(x,y)=(%3.1f,%3.1f)$',x(i),y(i)); ...
    'вторая строка, если надо';...
    'и ещё одна строка'});


%% Text effects

text(2.5,-0.5,{'Повёрнутый';'текст'},'Rotation',90);

text(0.5,-0.5,{'Цветной';'центрированный';'наклонный';'текст'},...
    'Color','r',...
    'HorizontalAlignment','center',...
    'FontAngle','italic');

text(1,-0.5,{'Крупный';'Шрифт:';'16pt'},'FontSize',16)

%% use epspad, otherwise it cuts a part of the x- & y-label 
matlabfrag('figs/example3ru','epspad',[10,22,10,10]);