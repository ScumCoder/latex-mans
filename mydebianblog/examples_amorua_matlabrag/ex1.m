%% example 1

close all ; clear all; clc;
set(0,'DefaultTextInterpreter', 'latex');
set(0,'DefaultAxesFontSize',12);
set(0,'DefaultTextFontSize',12);
%%
fig=myfigure(10); % width = 10cm

x=0:0.1:pi;
y=sin(x); plot(x,y,'r'); hold on;

z=cos(x); plot(x,z,'b');

xlabel('$t$, s'); ylabel('$l$, m');

l=legend('$\sin x$','$\cos x$');

%% Text
%i=6;
%plot(x(i),y(i),'or');
%text(x(i)+0.1,y(i),sprintf('Point $(x,y)=(%3.1f,%3.1f)$',x(i),y(i)));

text(0.5,-0.5,'$\int\limits_0^{2\pi}\sin x\, dx = 0$');

text(3,-0.5,'$\gamma = \frac{\alpha}{\zeta}$');

%% Use epspad, otherwise it cuts a part of the x- & y-label 
matlabfrag('figs/example1','epspad',[10,10,10,10]);