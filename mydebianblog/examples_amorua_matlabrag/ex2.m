%% example 2

close all ; clear all; clc;
set(0,'DefaultTextInterpreter', 'tex');
set(0,'DefaultAxesFontSize',12);
set(0,'DefaultTextFontSize',12);
%%
fig=myfigure(10); % width = 10cm

x=0:0.1:pi;
y=sin(x);
plot(x,y,'r'); hold on;

z=cos(x);
plot(x,z,'b');

xlabel('t, s', 'UserData','matlabfrag:$t$, s'); 
ylabel('l, m', 'UserData','matlabfrag:$l$, m');

l=mylegend({'sin x';'cos x'},...
           {'$\sin x$';'$\cos x$'});

% Alternatively use
%l=legend('sin x','cos x');
%lc=get(l,'children');
%set(lc(6),'UserData','matlabfrag:$\sin x$');
%set(lc(3),'UserData','matlabfrag:$\cos x$');

%%

%i=3;
%plot(x(i),y(i),'or');
%text(x(i)+0.1,y(i),sprintf('Point (x,y)=(%3.1f,%3.1f)',x(i),y(i)),...
%    'UserData',sprintf('matlabfrag:Point $(x,y)=(%3.1f,%3.1f)$',x(i),y(i)));

% we assume, in your tex file there is a command "\myint" defined
text(0.5,-0.5,'$\int\limits_0^{2\pi}\sin x\, dx = 0$','Interpreter','latex',...
     'UserData','matlabfrag:$\displaystyle\myint$ see eq.(\ref{eq:1})');

text(3,-0.5,'$\gamma = \frac{\alpha}{\zeta}$','Interpreter','latex',...
    'UserData','matlabfrag:$\displaystyle \gamma=\frac{\alpha}{\zeta}$'); 
 


%%
matlabfrag('figs/example2');