%% example 4

close all ; clear all; clc;
set(0,'DefaultTextInterpreter', 'latex');
set(0,'DefaultAxesFontSize',12);
set(0,'DefaultTextFontSize',12);
%%
fig=myfigure(10); % width = 10cm

x=0:0.1:pi;
y=sin(x);
plot(x,y,'r'); hold on;

z=cos(x);
plot(x,z,'b');

xlabel('$t$, s'); 
ylabel('$l$, m');

l=legend('$\sin x$','$\cos x$');

%% custom ticks
set(gca,'XLim',[0 pi],'XTick',[0 pi/2 pi],...
     'XTickLabel',{'$0$','$\pi/2$','$\pi$'});
 
%% use epspad, otherwise it cuts a part of the x- & y-label 
matlabfrag('figs/example4','epspad',[10,10,10,10]);